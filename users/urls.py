from django.urls import path

from users import views

#localhost:8000/users/create
urlpatterns = {
    path('', views.get_users),
    path('create', views.create_users),

    # localhost8000/users/
    path('get/<int:id>', views.get_user_by_id),

    path('delete/<int:id>', views.delete_user_by_id),

    path("update/<int:id>", views.update_user),

    path("activate/<int:id>", views.activate),

    path("deactivate/<int:id>", views.deactivate),

    path("changepass/<int:id>", views.change_password)
}